Role Name
=========

Role performs FAD6 checks on NCS540

Requirements
------------

No CISCO IOS-XR module

Role Variables
--------------

TBD

Dependencies
------------

Stand alone role, doesn't need any requirements. 
*parse_genie* can be used if needed

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    # Include role in playbook
      roles:
        - fad6
    
    # Run role in playbook
      tasks:
        - name: Run FAD6 checks
          include_role:
            name: fad6

License
-------

As Is, Open Source

Author Information
------------------

Stan Kozlov <skozlov@redhat.com>
